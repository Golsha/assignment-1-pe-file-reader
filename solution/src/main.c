/// @file
/// @brief Main application file

#include "pe_functions.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{


    (void) argc; (void) argv; // supress 'unused parameters' warning

    if(argc < 4)
        return 0;

    FILE* input_file = fopen(argv[1], "rb");
    FILE* output_file = fopen(argv[3], "wb");
    if (!input_file || !output_file) {
        fclose(input_file);
        fclose(output_file);
        return -1;
    }
    int flag_ans = 0;
    //fprintf(stderr, "dddd");
    if(get_section(input_file, output_file, argv[2])) {
        flag_ans = -1;
    }

    fclose(input_file);
    fclose(output_file);


    return flag_ans;
}
